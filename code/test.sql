BEGIN;

INSERT INTO Incidencias (id, tipo, radio, inicio, fin, geometry) VALUES (1460328181697, 'Incendio', 200, '2016-04-10T22:30+02:00', '2016-05-10T22:30+02:00', GeomFromText('POINT(-8.7373 42.0000)', 4326));

SELECT id, tipo, radio, inicio, fin, AsGeoJSON(geometry) FROM Incidencias;

COMMIT;

