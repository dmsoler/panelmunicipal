// ******************************************************************
// Panel Municipal v.0.5	 
// panelmunicipal.js
// Author: Dionisio Martínez Soler - dmsoler@gmail.com
// Date: 2016-06-06
// License: GPL version 3
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//*******************************************************************

// Default vars
if ( !zoomlevel ) {
	var zoomlevel = 13;
}
if ( !defaultlat ) {
	var defaultlat = 42.0000;
}
if ( !defaultlon ) {
	var defaultlon = -8.7373;
}
if ( !pnoa ) {
	var pnoa = true;
}
if ( !osm ) {
	var osm = true;
}
if ( !ocm ) {
	var ocm = false;
}
if ( !stoner ) {
	var stoner = false;
}
if ( !idepo ) {
	var idepo = true;
}
if ( !geojson ) {
	var geojson = false;
}

//	proj4js definition of EPSG:23029 (used by IDEPO): it can also be loaded in the .html with <script src="http://epsg.io/23029.js"></script>
proj4.defs("EPSG:23029","+proj=utm +zone=29 +ellps=intl +towgs84=-87,-98,-121,0,0,0,0 +units=m +no_defs");
var proj23029 = ol.proj.get('EPSG:23029');
proj23029.setExtent([448933.91,3860083.93,1860436.11,8381369.16]);

// The map
var map = new ol.Map({
  view: new ol.View({
	center: ol.proj.transform([defaultlon, defaultlat], 'EPSG:4326', 'EPSG:3857'),
    zoom: zoomlevel,
	projection: ol.proj.get('EPGS:3857')
  }),
  layers: [],
  renderer: 'canvas',
  target: 'map',
  controls: ol.control.defaults().extend([new ol.control.OverviewMap()])
});

// More controls for the map
// Zoom slider
zoomslider = new ol.control.ZoomSlider();
map.addControl(zoomslider);

// Toggle fullscreen: the 'fullscreen' element in the DOM will be the only one which is visible in fullscreen mode
fullscreen = new ol.control.FullScreen({source: 'fullscreen'});
map.addControl(fullscreen);

// Show mouse position in the map in EPSG:4326 by default
mouseposition = new ol.control.MousePosition({
	coordinateFormat: ol.coordinate.createStringXY(12),
	projection: 'EPSG:4326',
	target: 'mouseposition',
	undefinedHTML: '&nbsp;'
});
map.addControl(mouseposition);

// Change SRS used for mouse position coordinates, see 'mousepositionform' element in the DOM of the .html file
var projectionSelect = document.getElementById('srs');
projectionSelect.addEventListener('change', function(event) {
	mouseposition.setProjection(ol.proj.get(event.target.value));
});

// Scale line
scaleline = new ol.control.ScaleLine();
map.addControl(scaleline);

// Layers for the map
// PNOA
if ( pnoa ) {
	var pnoa = new ol.layer.Tile({
			source: new ol.source.TileWMS({
      		url: 'http://www.ign.es/wms-inspire/pnoa-ma',
      		params: {
        		'LAYERS': 'PNOA',
        		'FORMAT': 'image/jpeg',
				'VERSION': '1.1.1'
      		},
      		serverType: 'mapserver',
			attributions: [
			      new ol.Attribution({
			        html: 'Ortofotos del Plan Nacional de Ortofotografía Aérea (PNOA), servicio WMS de la <a href="http://www.idee.es" target="_blank">Infraestructura de Datos Espaciales de España</a> (IDEE).<br/>'
			      }),
					    ],
	//			minResolution: 2000,
    	})
    });

	pnoa.set('title', 'PNOA');

	map.addLayer(pnoa);
}

//OpenStreetMap
if ( osm ) {
	var osm = new ol.layer.Tile({
    	  source: new ol.source.OSM(),
		  opacity: 1,
	//	  minResolution: 200,
	//	  maxResolution: 2000
	});

	osm.set('title', 'OpenStreetMap');

	map.addLayer(osm);
}

//OpenCycleMap
if ( ocm ) {
	var ocm = new ol.layer.Tile({
		source: new ol.source.OSM({
		    attributions: [
				new ol.Attribution({
			        html: 'All maps &copy; '+'<a href="http://www.opencyclemap.org/">OpenCycleMap</a>'
				}),
				ol.source.OSM.ATTRIBUTION
			],
		    url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png'
		}),
		opacity: 1
	});

	ocm.set('title', 'OpenCycleMap');

	map.addLayer(ocm);
}

//Stamen toner saver layer for printing
if ( stoner ) {
	var stoner = new ol.layer.Tile({
    	  source: new ol.source.Stamen({layer: 'toner'}),
		  opacity: 1,
	//	  minResolution: 200,
	//	  maxResolution: 2000
	});

	stoner.set('title', 'Stamen toner');

	map.addLayer(stoner);
}

//IDEPO
if ( idepo ) {
      var image = new ol.style.Circle({
        radius: 5,
        fill: null,
        stroke: new ol.style.Stroke({color: 'red', width: 1})
      });
      var styles = {
        'Point': new ol.style.Style({
          image: image
        }),
        'LineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'green',
            width: 1
          })
        }),
        'MultiLineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'green',
            width: 1
          })
        }),
        'MultiPoint': new ol.style.Style({
          image: image
        }),
        'MultiPolygon': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'yellow',
            width: 1
          }),
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0.1)'
          })
        }),
        'Polygon': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'blue',
            lineDash: [4],
            width: 3
          }),
          fill: new ol.style.Fill({
            color: 'rgba(0, 0, 255, 0.1)'
          })
        }),
        'GeometryCollection': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'magenta',
            width: 2
          }),
          fill: new ol.style.Fill({
            color: 'magenta'
          }),
          image: new ol.style.Circle({
            radius: 10,
            fill: null,
            stroke: new ol.style.Stroke({
              color: 'magenta'
            })
          })
        }),
        'Circle': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'red',
            width: 2
          }),
          fill: new ol.style.Fill({
            color: 'rgba(255,0,0,0.2)'
          })
        })
      };
      var styleFunction = function(feature) {
        return styles[feature.getGeometry().getType()];
      };

	var idepoCollection = new ol.Collection(idepogeojson);

	var geojsonformat = new ol.format.GeoJSON();

	var features = new ol.Collection();

	idepoCollection.forEach( function(geojsonobject) {
		features.extend(geojsonformat.readFeatures(geojsonobject, {dataProjection: proj23029, featureProjection: map.getView().getProjection()}));
	});

	var idepoSource = new ol.source.Vector({
        features: features,
		attributions: [
			      new ol.Attribution({
			        html: '<br/>Datos espaciales: &copy; <a href="http://ide.depo.es" target="_blank">Deputación de Pontevedra</a>, bajo una <a href="http://creativecommons.org/licenses/by/3.0/deed.es_ES" target="_blank">Licencia Creative Commons Reconocimiento 3.0 Unported</a>'
			      		})]
      });

	var idepoLayer = new ol.layer.Vector({
        source: idepoSource,
        style: styleFunction
      });

	idepoLayer.set('title', 'IDEPO');

	map.addLayer(idepoLayer);

	idepoLayer.setVisible(false);
}

//Function to create a layer switcher for toggling visibility and transparency of the layers
function visibility(layer, i){
	var docfrag=document.createDocumentFragment();
	var ul=docfrag.appendChild(document.createElement("ul"));
	var li=ul.appendChild(document.createElement("li"));
	var input0 = li.appendChild(document.createElement("input"));
	input0.setAttribute("id", layer.get('title')+"Visibility");
	input0.setAttribute("type", "checkbox");
	if ( layer.getVisible() ) {
		input0.setAttribute("checked", "checked");
	}
	li.appendChild(document.createTextNode(i+" - "+layer.get('title')+" "));
	li.appendChild(document.createElement("br"));
	var input1 = li.appendChild(document.createElement("input"));
	input1.setAttribute("id", layer.get('title')+"Opacity");
	input1.setAttribute("type", "range");
	input1.setAttribute("min", "0");
	input1.setAttribute("max", "1");
	input1.setAttribute("step", "0.1");
	document.getElementById("layerswitcher").appendChild(docfrag);
	document.getElementById(layer.get('title')+"Visibility").addEventListener('change', function() {
  		var checked = this.checked;
  		if (checked !== layer.getVisible()) {
    		layer.setVisible(checked);
  		}
	});
	document.getElementById(layer.get('title')+"Opacity").addEventListener('change', function() {
  		layer.setOpacity(this.value);
	});
}

//Apply visibility function to all base layers in the map
map.getLayers().forEach(visibility);

// Add a layer for the content of database 'Incidencias'
var incidenciasStyle = new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
            color: '#ffcc33',
            width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#ffcc33'
            })
          })
});
var incidencias = new ol.Collection();
geojsonformat = new ol.format.GeoJSON();
var incidenciasSource = new ol.source.Vector({features: incidencias, url: '/cgi-bin/incidenciaslayer.py', format: geojsonformat});
var incidenciasLayer = new ol.layer.Vector({
    source: incidenciasSource,
    style: incidenciasStyle,
	title: 'Incidencias'
});
var buffer = new ol.Collection();
var incidenciasBufferSource = new ol.source.Vector({features: buffer, url: '/cgi-bin/incidenciasbuffer.py', format: geojsonformat});
var incidenciasBufferLayer = new ol.layer.Vector({
    source: incidenciasBufferSource,
    style: incidenciasStyle,
	title: 'Áreas afectadas'
});
map.addLayer(incidenciasLayer);
map.addLayer(incidenciasBufferLayer);
visibility(incidenciasLayer, 'Base de datos');
visibility(incidenciasBufferLayer, 'Base de datos');

//Add a filter for the content of database
var filter=document.createDocumentFragment();
filter.appendChild(document.createTextNode(' Aplicar filtro a la base de datos: '));
var filterbutton = filter.appendChild(document.createElement("input"));
filterbutton.setAttribute("id", "filterbutton");
filterbutton.setAttribute("type", "submit");
filterbutton.setAttribute("value", "Aplicar");
var filtertextarea = filter.appendChild(document.createElement("textarea"));
filtertextarea.setAttribute("id", "filtertextarea");
filtertextarea.setAttribute("name", "filter");
filtertextarea.setAttribute("style", "width:100%; height:2em;");
filtertextarea.appendChild(document.createTextNode('inicio < date(\'now\') AND fin > date(\'now\')'));
document.getElementById("layerswitcher").appendChild(filter);
document.getElementById("filterbutton").addEventListener('click', function() {
		var query = '/cgi-bin/incidenciaslayer.py?query=' + $('#filtertextarea').val();
		var querybuffer = '/cgi-bin/incidenciasbuffer.py?query=' + $('#filtertextarea').val();
		incidenciasSource = new ol.source.Vector({features: incidencias, url: query, format: geojsonformat});
		incidenciasBufferSource = new ol.source.Vector({features: buffer, url: querybuffer, format: geojsonformat});
		reloadDatabase();
});

//Add a layer for modified features through interactions
var incidenciasModificadas = new ol.Collection();
var incidenciasModificadasSource = new ol.source.Vector({features: incidenciasModificadas});
var incidenciasModificadasLayer = new ol.layer.Vector({
    source: incidenciasModificadasSource,
    style: incidenciasStyle,
	title: 'Incidencias modificadas'
});

drawInteraction = new ol.interaction.Draw({
          features: incidencias,
          type: 'Point',
		  condition: ol.events.condition.noModifierKeys
});

drawInteraction.on('drawend', function(event) {
		var date = new Date();
		var timestamp = date.getTime();	
		event.feature.setId(timestamp); 
		event.feature.set('id', timestamp);
		newfeature = event.feature;
		incidenciasModificadasLayer.getSource().clear();
		incidenciasModificadasLayer.getSource().addFeature(newfeature);
		document.getElementById('info').innerHTML = '';
		var docfrag=document.createDocumentFragment();
		docfrag.appendChild(document.createTextNode('Id: '+event.feature.getId()));
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Coordenadas (EPSG:4326): '+ol.proj.transform(event.feature.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326')));
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Tipo: '));
		var input0 = docfrag.appendChild(document.createElement("input"));
		input0.setAttribute("name", "tipo");
		input0.setAttribute("type", "text");
		input0.setAttribute("size", 50);
		input0.setAttribute("value", "Incidencia");
		event.feature.set('tipo', 'Incidencia');
		input0.addEventListener('change', function() {
			event.feature.set('tipo', this.value);
			newfeature.set('tipo', this.value);
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Radio (en metros): '));
		var input1 = docfrag.appendChild(document.createElement("input"));
		input1.setAttribute("name", "radio");
		input1.setAttribute("type", "text");
		input1.setAttribute("size", 5);
		input1.setAttribute("value", 0);
		event.feature.set('radio', 0);
		input1.addEventListener('change', function() {
			event.feature.set('radio', this.value);
			newfeature.set('radio', this.value);
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Inicio: '));
		var input2 = docfrag.appendChild(document.createElement("input"));
		input2.setAttribute("name", "inicio");
		input2.setAttribute("type", "text");
		input2.setAttribute("size", 22);
		var datetz = date.toISOString();
		if(date.getTimezoneOffset() != 0){
		    date.setTime ( date.getTime() - date.getTimezoneOffset()*60*1000 );
			var sign;
			if ( date.getTimezoneOffset() < 0 ) { 
				sign = '+';
			} else {
				 sign = '-';
			}
			var hourtz = date.getTimezoneOffset()/-60;
			if (hourtz < 10) {
				hourtz = '0' + hourtz;
			}
			datetz = date.toISOString().substr(0,16) + sign + hourtz + ':00';
		}
		input2.setAttribute("value", datetz);
		event.feature.set('inicio', datetz);
		input2.addEventListener('change', function() {
			event.feature.set('inicio', this.value);
			newfeature.set('inicio', this.value);
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Fin: '));
		var input3 = docfrag.appendChild(document.createElement("input"));
		input3.setAttribute("name", "fin");
		input3.setAttribute("type", "text");
		input3.setAttribute("size", 22);
		input3.setAttribute("value", datetz);
		event.feature.set('fin', datetz);
		input3.addEventListener('change', function() {
			event.feature.set('fin', this.value);
			newfeature.set('fin', this.value);
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
        document.getElementById('info').appendChild(docfrag);
});

// Select interaction
var incidenciasSelected = new ol.Collection();
var selectInteraction = new ol.interaction.Select({
		condition: ol.events.condition.doubleClick,
		toggleCondition: ol.events.condition.platformKeyOnly,
		layers: [incidenciasLayer],
		features: incidenciasSelected		
});

selectInteraction.on('select', function(event) {
		document.getElementById('info').innerHTML = '';
		var docfrag=document.createDocumentFragment();
		docfrag.appendChild(document.createTextNode(' Incidencia seleccionada(' + event.target.getFeatures().getLength() + '): ' + event.target.getFeatures().item(0).getId() ));
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Coordenadas (EPSG:4326): '+ol.proj.transform(event.target.getFeatures().item(0).getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326')));
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Tipo: '));
		var input0 = docfrag.appendChild(document.createElement("input"));
		input0.setAttribute("name", "tipo");
		input0.setAttribute("type", "text");
		input0.setAttribute("size", 50);
		input0.setAttribute("value", event.target.getFeatures().item(0).get('tipo'));
		input0.addEventListener('change', function() {
			var value = this.value;
			var id = event.target.getFeatures().item(0).getId();
			incidenciasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('tipo', value);
				}
			});
			var modified = false;
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					modified = true;
				}
			});
			if ( !modified ) {
				incidenciasModificadasSource.addFeature(event.target.getFeatures().item(0));
			}
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('tipo', value);
				}
			});
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Radio (en metros): '));
		var input1 = docfrag.appendChild(document.createElement("input"));
		input1.setAttribute("name", "radio");
		input1.setAttribute("type", "text");
		input1.setAttribute("size", 5);
		input1.setAttribute("value", event.target.getFeatures().item(0).get('radio'));
		input1.addEventListener('change', function() {
			var value = this.value;
			var id = event.target.getFeatures().item(0).getId();
			incidenciasModificadasSource.addFeature(event.target.getFeatures().item(0));
			incidenciasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('radio', value);
				}
			});
			var modified = false;
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					modified = true;
				}
			});
			if ( !modified ) {
				incidenciasModificadasSource.addFeature(event.target.getFeatures().item(0));
			}
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('radio', value);
				}
			});
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Inicio: '));
		var input2 = docfrag.appendChild(document.createElement("input"));
		input2.setAttribute("name", "inicio");
		input2.setAttribute("type", "text");
		input2.setAttribute("size", 22);
		input2.setAttribute("value", event.target.getFeatures().item(0).get('inicio'));
		input2.addEventListener('change', function() {
			var value = this.value;
			var id = event.target.getFeatures().item(0).getId();
			incidenciasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('inicio', value);
				}
			});
			var modified = false;
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					modified = true;
				}
			});
			if ( !modified ) {
				incidenciasModificadasSource.addFeature(event.target.getFeatures().item(0));
			}
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('inicio', value);
				}
			});
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		docfrag.appendChild(document.createTextNode('Fin: '));
		var input3 = docfrag.appendChild(document.createElement("input"));
		input3.setAttribute("name", "fin");
		input3.setAttribute("type", "text");
		input3.setAttribute("size", 22);
		input3.setAttribute("value", event.target.getFeatures().item(0).get('fin'));
		input3.addEventListener('change', function() {
			var value = this.value;
			var id = event.target.getFeatures().item(0).getId();
			incidenciasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('fin', value);
				}
			});
			var modified = false;
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					modified = true;
				}
			});
			if ( !modified ) {
				incidenciasModificadasSource.addFeature(event.target.getFeatures().item(0));
			}
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					sourcefeature.set('fin', value);
				}
			});
			save(incidenciasModificadasLayer,'EPSG:4326');
		});
		docfrag.appendChild(document.createElement("br"));
		var deletebutton = docfrag.appendChild(document.createElement("input"));
		deletebutton.setAttribute("id", "DeleteButton");
		deletebutton.setAttribute("type", "submit");
		deletebutton.setAttribute("value", "Borrar la incidencia seleccionada");
		deletebutton.addEventListener('click', function() {
			var id = event.target.getFeatures().item(0).getId();
    		$.post("/cgi-bin/incidencias.py", {erase: id}, function(data, status){
        		document.getElementById('databasemessages').innerHTML = data;
    		});
			incidenciasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					incidenciasLayer.getSource().removeFeature(sourcefeature);
				}
			});
			incidenciasModificadasLayer.getSource().getFeatures().forEach(function(sourcefeature) {
				if (sourcefeature.getId() === id) {
					incidenciasModificadasLayer.getSource().removeFeature(sourcefeature);
				}
			});
			document.getElementById('info').innerHTML = '';
			reloadDatabase();
		});
        document.getElementById('info').appendChild(docfrag);
});

//(De)activation of select and draw interactions
var activeinteraction=document.createDocumentFragment();
activeinteraction.appendChild(document.createTextNode(' (Des)activar edición de la base de datos: '));
var activeinteractioninput = activeinteraction.appendChild(document.createElement("input"));
activeinteractioninput.setAttribute("id", incidenciasLayer.get('title')+"Interaction");
activeinteractioninput.setAttribute("type", "checkbox");
map.getInteractions().forEach(function(interaction) {
	if ( interaction == drawInteraction || interaction == selectInteraction ) {
		activeinteractioninput.setAttribute("checked", "checked");
	}
}
);
document.getElementById("layerswitcher").appendChild(activeinteraction);
document.getElementById(incidenciasLayer.get('title')+"Interaction").addEventListener('change', function() {
  	var checked = this.checked;
  	if ( !checked ) {
		map.removeInteraction(drawInteraction);
		map.removeInteraction(selectInteraction);
		document.getElementById('info').innerHTML = '';
		document.getElementById('databasemessages').innerHTML = '';
		incidenciasModificadasLayer.getSource().clear();
		map.addOverlay(overlay);
	}
	else {
    	map.addInteraction(drawInteraction);
		map.addInteraction(selectInteraction);
		overlay = map.removeOverlay(overlay);
	}
});

//Update database
var updatedatabase = document.createDocumentFragment();
var geojsondatatextarea = updatedatabase.appendChild(document.createElement("textarea"));
geojsondatatextarea.setAttribute("id", "geojsondata");
geojsondatatextarea.setAttribute("name", "geojsondata");
if ( geojson ) {
	geojsondatatextarea.setAttribute("style", "width:100%;");
} else {
	geojsondatatextarea.setAttribute("style", "display:none;");
}
var databasemessages = updatedatabase.appendChild(document.createElement("div"));
databasemessages.setAttribute("id", "databasemessages");
document.getElementById("layerswitcher").appendChild(updatedatabase);

//Export visible features from the database to a KML file
var exportkml = document.createDocumentFragment();
var exportform = exportkml.appendChild(document.createElement("form"));
exportform.setAttribute("id", "ExportForm");
exportform.setAttribute("method", "post");
exportform.setAttribute("action", "/cgi-bin/incidenciaskml.py");
exportform.setAttribute("target", "_blank");
var exporttextarea = exportform.appendChild(document.createElement("textarea"));
exporttextarea.setAttribute("id", "kml");
exporttextarea.setAttribute("name", "kml");
exporttextarea.setAttribute("style", "display:none;");
var exportbutton = exportkml.appendChild(document.createElement("input"));
exportbutton.setAttribute("id", "ExportButton");
exportbutton.setAttribute("type", "submit");
exportbutton.setAttribute("value", "Exportar incidencias visibles a un fichero KML");
exportbutton.addEventListener('click', function() {
	var kmlformat = new ol.format.KML();
	var features = new ol.Collection();
	features.extend(incidenciasLayer.getSource().getFeatures());
	features.extend(incidenciasBufferLayer.getSource().getFeatures());
	var kmldata = kmlformat.writeFeatures(features.getArray(),{dataProjection: 'EPSG:4326', featureProjection: map.getView().getProjection()});
	$('#kml').val(kmldata);
	$('#ExportForm').submit();
});
document.getElementById("layerswitcher").appendChild(exportkml);

//Save vector layer data
function save(layer, projection) {
	var format = new ol.format.GeoJSON();
	var data = format.writeFeatures(layer.getSource().getFeatures(),{dataProjection: projection, featureProjection: map.getView().getProjection()});
	if(typeof(Storage) !== "undefined") {
    	localStorage.setItem(layer.get('title'), data);
		$('#geojsondata').val(localStorage.getItem(layer.get('title'))); 
	} else {
		$('#geojsondata').val(data);
	}
    $.post("/cgi-bin/incidencias.py", {geojsondata: $('#geojsondata').val()}, function(data, status){
        document.getElementById('databasemessages').innerHTML = data;
    });
	reloadDatabase();
}

//Reload database
function reloadDatabase() {
	incidenciasLayer.getSource().clear();
	incidenciasBufferLayer.getSource().clear();
	incidenciasLayer.setSource(incidenciasSource);
	incidenciasBufferLayer.setSource(incidenciasBufferSource);
};

// Display fields of the features in a popup
var popupcontainer = document.getElementById('popup');
var popupcontent = document.getElementById('popup-content');
var popupcloser = document.getElementById('popup-closer');
var overlay;
overlay = new ol.Overlay( ({
    element: popupcontainer,
    autoPan: true,
    autoPanAnimation: {
       duration: 250
    }
}));

popupcloser.onclick = function() {
        overlay.setPosition(undefined);
        popupcloser.blur();
		popupcontent.innerHTML = '';
        return false;
      };

map.addOverlay(overlay);

function displayFeatureInfo(pixel,coordinate) {
		var features = [];
        map.forEachFeatureAtPixel(pixel, function(feature,layer) {
			if (layer.get('title') == 'Incidencias' || layer.get('title') == 'IDEPO' ) {
				feature.set('layer', layer.get('title'));          
				features.push(feature);
			}
        });
		var html = document.createDocumentFragment();
		popupcontent.innerHTML = '';
        if (features.length > 0) {
          var i, ii;
          for (i = 0, ii = features.length; i < ii; ++i) {
			html.appendChild(document.createTextNode('Capa ' + features[i].get('layer')));
			html.appendChild(document.createElement("br"));
			if ( features[i].get('layer') == 'Incidencias' ) {
				html.appendChild(document.createTextNode('Id: ' + features[i].get('id')));
				html.appendChild(document.createElement("br"));
				html.appendChild(document.createTextNode('Tipo: ' + features[i].get('tipo')));
				html.appendChild(document.createElement("br"));
				html.appendChild(document.createTextNode('Inicio: ' + features[i].get('inicio')));
				html.appendChild(document.createElement("br"));
				html.appendChild(document.createTextNode('Fin:    ' + features[i].get('fin')));
				html.appendChild(document.createElement("br"));
			} else {
				var keys = features[i].getKeys();
				for (j = 0, jj = keys.length; j < jj; ++j) {
					if ( keys[j] != 'geometry' && keys[j] != 'layer' ) {
						html.appendChild(document.createTextNode(keys[j] + ': ' + features[i].get(keys[j])));
						html.appendChild(document.createElement("br"));
					}
				}
			}
          }
		  popupcontent.appendChild(html);
          overlay.setPosition(coordinate);
        } 
};

map.on('singleclick', function(evt) {
       displayFeatureInfo(evt.pixel,evt.coordinate);
});

