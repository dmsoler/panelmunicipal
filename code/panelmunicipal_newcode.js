if ( !idepo ) {
	var idepo = false;
}

var proj23029 = ol.proj.get('EPSG:23029');
proj23029.setExtent([230080.3265, 4004820.9359, 769919.6735, 6655361.1880]);

var style = {
  'Point': [new ol.style.Style({
    image: new ol.style.Circle({
      fill: new ol.style.Fill({
        color: 'rgba(255,255,0,0.4)'
      }),
      radius: 5,
      stroke: new ol.style.Stroke({
        color: '#ff0',
        width: 1
      })
    })
  })],
  'LineString': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: '#f00',
      width: 3
    })
  })],
  'MultiLineString': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: '#0f0',
      width: 3
    })
  })]
};

if ( idepo ) {

	ideposource = new ol.source.Vector({
		loader: function(extent) {

		var extent23029 = ol.proj.transformExtent(extent, 'EPSG:3857', 'EPSG:23029');

		$.ajax('http://ide.depo.es:8080/geoserver/idepo/wfs',{
			type: 'GET',
			data: {
				service: 'WFS',
				version: '1.1.0',
				request: 'GetFeature',
				typename: 'idepo:instal_deportiva',
				srsname: 'EPSG:23029',
				bbox: extent23029.join(',') + ',EPSG:23029'
				},
			}).done(loadFeatures);
		},
		strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
			maxZoom: 19
			})),
		attributions: [
			      new ol.Attribution({
			        html: '<br/>Datos espaciales: &copy; <a href="http://ide.depo.es" target="_blank">Deputación de Pontevedra</a>, bajo una <a href="http://creativecommons.org/licenses/by/3.0/deed.es_ES" target="_blank">Licencia Creative Commons Reconocimiento 3.0 Unported</a>'
					})
		]
	});

	window.loadFeatures = function(response) {
    	formatWFS = new ol.format.WFS(),
    	sourceVector.addFeatures(formatWFS.readFeatures(response, {dataProjection:'EPSG:23029',featureProjection:'EPSG:3857'}));
    };

/*********** Without jQuery ************
	var idepoformat = new ol.format.GeoJSON();

//	var idepoformat = new ol.format.GML();

//	var idepourl = 'http://ide.depo.es:8080/geoserver/idepo/wfs?SERVICE=WFS&VERSION=1.0.0&REQUEST=GetFeature&TYPENAME=idepo:infraestr_viaria'; //&SRSNAME=EPSG:23029';

	var idepourl = function(extent) {
		var extent23029 = ol.proj.transformExtent(extent, 'EPSG:3857', 'EPSG:23029');

          return 'http://ide.depo.es:8080/geoserver/idepo/wfs?service=WFS&' +
              'version=1.1.0&request=GetFeature&typename=idepo:infraestr_viaria&' +
              'outputFormat=json&SRSNAME=EPSG:23029&' +
              'bbox=' + extent23029.join(',') + ',EPSG:23029';
        };


//	var idepourl = 'http://ide.depo.es:8080/geoserver/idepo/wfs?service=WFS&version=1.1.0&request=GetFeature&typename=idepo:infraestr_viaria&outputFormat=json&srsname=EPSG:23029';

	document.write(idepourl);

	var ideposource = new ol.source.Vector({
		projection: proj23029,      
		format: idepoformat,
        url: idepourl,
        strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
          maxZoom: 19
        })),
		attributions: [
			      new ol.Attribution({
			        html: '<br/>Datos espaciales: &copy; <a href="http://ide.depo.es" target="_blank">Deputación de Pontevedra</a>, bajo una <a href="http://creativecommons.org/licenses/by/3.0/deed.es_ES" target="_blank">Licencia Creative Commons Reconocimiento 3.0 Unported</a>'
					})
		]
      });
*************/

     var idepo = new ol.layer.Vector({
        source: ideposource,
 		style: style
/*      style: new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: 'rgba(255, 0, 0, 1.0)',
            width: 20
          })
        })
*/
      });

	idepo.set('title', 'IDEPO');

	map.addLayer(idepo);
}

//WKT
var wkt = new ol.format.WKT(),
    vectorLayer,
    source,
    features = [],
    feature,
    markers;

$.ajax('test.xml').then(function(response) {
    var markers = response.getElementsByTagName('marker');
    for (var i = 0; i < markers.length; i++) {
        feature=wkt.readFeature(markers[i].attributes.geometry.nodeValue);
        features.push(feature);
    }
    source = new ol.source.Vector({
        features: features
    });
    vectorLayer = new ol.layer.Vector({
        source: source
    });
    map.addLayer(vectorLayer);    
});

var format = new ol.format.WKT();
      var feature = format.readFeature(
          'POLYGON((10.689697265625 -25.0927734375, 34.595947265625 ' +
              '-20.1708984375, 38.814697265625 -35.6396484375, 13.502197265625 ' +
              '-39.1552734375, 10.689697265625 -25.0927734375))');
      feature.getGeometry().transform('EPSG:4326', 'EPSG:3857');

      var vector = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [feature]
        }),
		title: 'WKT vector'
      });

map.addLayer(vector);

// Add a new drag and drop layer to the map
// Default style for all vector features
var defaultStyle = {
        'Point': new ol.style.Style({
          image: new ol.style.Circle({
            fill: new ol.style.Fill({
              color: 'rgba(255,255,0,0.5)'
            }),
            radius: 5,
            stroke: new ol.style.Stroke({
              color: '#ff0',
              width: 1
            })
          })
        }),
        'LineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: '#f00',
            width: 3
          })
        }),
        'Polygon': new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(0,255,255,0.5)'
          }),
          stroke: new ol.style.Stroke({
            color: '#0ff',
            width: 1
          })
        }),
        'MultiPoint': new ol.style.Style({
          image: new ol.style.Circle({
            fill: new ol.style.Fill({
              color: 'rgba(255,0,255,0.5)'
            }),
            radius: 5,
            stroke: new ol.style.Stroke({
              color: '#f0f',
              width: 1
            })
          })
        }),
        'MultiLineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: '#0f0',
            width: 3
          })
        }),
        'MultiPolygon': new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(0,0,255,0.5)'
          }),
          stroke: new ol.style.Stroke({
            color: '#00f',
            width: 1
          })
        })
};

// Add style to features
var styleFunction = function(feature, resolution) {
        var featureStyleFunction = feature.getStyleFunction();
        if (featureStyleFunction) {
          return featureStyleFunction.call(feature, resolution);
        } else {
          return defaultStyle[feature.getGeometry().getType()];
        }
};

// Create a drag and drop interaction for adding vector features to the map
var dragAndDropInteraction = new ol.interaction.DragAndDrop({
        formatConstructors: [
          ol.format.GPX,
          ol.format.GeoJSON,
          ol.format.IGC,
          ol.format.KML,
          ol.format.TopoJSON
        ]
});

dragAndDropInteraction.on('addfeatures', function(event) {
        var vectorSource = new ol.source.Vector({
          features: event.features
        });
		var imageLayer = new ol.layer.Image({
          	source: new ol.source.ImageVector({            
				source: vectorSource,
            	style: styleFunction
          	}),
		  	title: 'Drag and drop'
        });
        map.addLayer(imageLayer);
		visibility(imageLayer, 'Fichero');
        map.getView().fit(
            vectorSource.getExtent(), /** @type {ol.Size} */ (map.getSize()));
});

// Display 'id' field of the features
var displayFeatureInfo = function(pixel) {
        var features = [];
        map.forEachFeatureAtPixel(pixel, function(feature) {
          features.push(feature);
        });
        if (features.length > 0) {
          var info = [];
          var i, ii;
          for (i = 0, ii = features.length; i < ii; ++i) {
            info.push(features[i].get('id'));
          }
          document.getElementById('info').innerHTML = info.join(', ') || '&nbsp';
        } else {
          document.getElementById('info').innerHTML = '&nbsp;';
        }
};

map.on('pointermove', function(evt) {
        if (evt.dragging) {
          return;
        }
        var pixel = map.getEventPixel(evt.originalEvent);
        displayFeatureInfo(pixel);
});

map.on('click', function(evt) {
       displayFeatureInfo(evt.pixel);
});

//Update database
var updatedatabase = document.createDocumentFragment();
var geojsondatatextarea = updatedatabase.appendChild(document.createElement("textarea"));
geojsondatatextarea.setAttribute("id", "geojsondata");
geojsondatatextarea.setAttribute("name", "geojsondata");
if ( geojson ) {
	geojsondatatextarea.setAttribute("style", "width:100%;");
} else {
	geojsondatatextarea.setAttribute("style", "display:none;");
}
var updatedatabasebutton = updatedatabase.appendChild(document.createElement("input"));
updatedatabasebutton.setAttribute("id", "UpdateDatabase");
updatedatabasebutton.setAttribute("type", "submit");
updatedatabasebutton.setAttribute("value", "Guardar incidencias nuevas y modificadas");
document.getElementById("layerswitcher").appendChild(updatedatabase);
document.getElementById("UpdateDatabase").addEventListener('click', function() {
    $.post("/cgi-bin/incidencias.py", {geojsondata: $('#geojsondata').val()}, function(data, status){
        alert(data);
    });
	incidenciasLayer.getSource().clear();
	incidenciasBufferLayer.getSource().clear();
	incidenciasLayer.setSource(incidenciasSource);
	incidenciasBufferLayer.setSource(incidenciasBufferSource);
});


