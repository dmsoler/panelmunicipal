#!/usr/bin/env python2.7
""" incidenciaskml.py: Panel Municipal v.0.5."""
import cgi, cgitb

#Uncomment the following line for debugging cgi script
cgitb.enable()

sentdata = cgi.FieldStorage()

if sentdata.getvalue('kml'):
    print "Content-Disposition: attachment; filename=\"incidencias.kml\""
    print "Content-type: application/vnd.google-earth.kml+xml\n"
    print str(sentdata.getvalue('kml'))
else:
    print "Content-type: text/html; charset=utf-8\n\n"
    print "No hay datos para exportar."


