#!/usr/bin/env python2.7
""" incidencias.py: Panel Municipal v.0.5."""
import cgi, cgitb, geojson
from pyspatialite import dbapi2 as db

#Uncomment the following line for debugging cgi script
#cgitb.enable()

sentdata = cgi.FieldStorage()

conn = db.connect('incidencias.sqlite')

with conn:
    cur = conn.cursor()
    if sentdata.getvalue('geojsondata'):
        data = sentdata.getvalue('geojsondata')
        jsondata = geojson.loads(data)
        for feature in jsondata['features']:
            cur.execute('SELECT id FROM Incidencias WHERE id = ' + \
                        str(feature['properties']['id']))
            test = cur.fetchall()
            if len(test) > 0:
                sql = 'UPDATE Incidencias SET tipo = \'' + \
                      unicode(feature['properties']['tipo']).encode('utf-8') + \
                      '\', radio = ' + str(feature['properties']['radio']) + \
                      ', inicio = \'' + str(feature['properties']['inicio']) + \
                      '\', fin = \'' + str(feature['properties']['fin']) + \
                      '\', geometry = GeomFromText(\'POINT(' + \
                      str(feature['geometry']['coordinates'][0]) + ' ' + \
                      str(feature['geometry']['coordinates'][1]) + ')\', 4326)' + \
                      ' WHERE id = ' + str(feature['properties']['id']) + ';'
                cur.execute(sql)
                msg = '\nIncidencia ' + str(feature['properties']['id']) + ' modificada.'
                print msg
            else:
                sql = 'INSERT INTO Incidencias (id, tipo, radio, inicio, fin, geometry) VALUES (' + \
                      str(feature['properties']['id']) + ', \'' + \
                      unicode(feature['properties']['tipo']).encode('utf-8') + '\', ' + \
                      str(feature['properties']['radio']) + ', \'' + \
                      str(feature['properties']['inicio']) + '\', \'' + \
                      str(feature['properties']['fin']) + '\', ' + 'GeomFromText(\'POINT(' + \
                      str(feature['geometry']['coordinates'][0]) + ' ' + \
                      str(feature['geometry']['coordinates'][1]) + ')\', 4326));'
                cur.execute(sql)
                msg = '\nIncidencia ' + str(feature['properties']['id']) + ' guardada.'
                print msg        
    if sentdata.getvalue('erase'):
        id = sentdata.getvalue('erase')
        cur = conn.cursor()
        sql = 'DELETE FROM Incidencias WHERE id=' + id + ';'
        cur.execute(sql)
        msg = '\nIncidencia ' + id + ' borrada.'
        print msg
conn.close()

quit()
