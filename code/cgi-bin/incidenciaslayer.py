#!/usr/bin/env python2.7
""" incidenciaslayer.py: Panel Municipal v.0.5."""
import cgi, cgitb, geojson
from pyspatialite import dbapi2 as db
from geojson import Point, Feature, FeatureCollection

#Uncomment the following line for debugging cgi script
#cgitb.enable()

sentdata = cgi.FieldStorage()

conn = db.connect('incidencias.sqlite')

featureCollection = list()

with conn:
    cur = conn.cursor()
    sql = 'SELECT id, tipo, radio, inicio, fin, AsGeoJSON(geometry) FROM Incidencias'
    if sentdata.getvalue('query'):
        query = sentdata.getvalue('query')
        sql = sql + ' WHERE ' + query
    rs = cur.execute(sql).fetchall()
    for row in rs:
        point = geojson.loads(row[5])
        geopoint = Point((point['coordinates'][0], point['coordinates'][1]))
        feature = Feature(geometry=geopoint,
                          id=row[0],
                          properties={"id": row[0],
                                      "tipo": row[1],
                                      "radio": row[2],
                                      "inicio": row[3],
                                      "fin": row[4]})
        featureCollection.append(feature)

conn.close()

featureCollection = FeatureCollection(featureCollection)

print "Content-type: application/json\n"
print geojson.dumps(featureCollection, sort_keys=False, indent=2)

quit()
