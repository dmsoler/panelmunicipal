#!/usr/bin/env python2.7
""" incidenciasbuffer.py: Panel Municipal v.0.5."""
import cgi, cgitb, geojson
from pyspatialite import dbapi2 as db
from geojson import Point, Polygon, Feature, FeatureCollection

#Uncomment the following line for debugging cgi script
#cgitb.enable()

sentdata = cgi.FieldStorage()

conn = db.connect('incidencias.sqlite')

featureCollection = list()

with conn:
    cur = conn.cursor()
    sql = 'SELECT id,tipo,inicio,fin,AsGeoJSON(Transform(Buffer(Transform(geometry, 23029), radio), 4326)) FROM Incidencias WHERE radio <> 0'
    if sentdata.getvalue('query'):
        query = sentdata.getvalue('query')
        sql = sql + ' AND ' + query
    rs = cur.execute(sql).fetchall()
    for row in rs:
        polygon = geojson.loads(row[4])
        geopolygon = Polygon(polygon['coordinates'])
        idbuffer = str(row[0])+'buffer'
        feature = Feature(geometry=geopolygon,
                          id=idbuffer,
                          properties={"id": idbuffer,
                                      "tipo": row[1],
                                      "inicio": row[2],
                                      "fin": row[3]})
        featureCollection.append(feature)

conn.close()

featureCollection = FeatureCollection(featureCollection)

print "Content-type: application/json\n"
print geojson.dumps(featureCollection, sort_keys=False, indent=2)

quit()
