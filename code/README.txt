Panel Municipal v.0.5.
======================

La aplicación Panel Municipal es una aplicación web con dos componentes:
un componente cliente escrito en HTML5+JavaScript+CSS, y un componente
servidor escrito en Python y que debe funcionar en cualquier servidor
web con soporte CGI y un intérprete Python con los módulos pyspatialite
y geojson. La versión de Python debe ser la 2.7, la última que soporta
el módulo pyspatialite.

Para poder ponerla en funcionamiento se dispone de tres scripts sencillos
que crean una base de datos Spatialite, verifican su funcionamiento e
introducen en ella un registro de prueba, e inician un servidor web CGI 
mediante el módulo CGIHTTPServer de Python.

Los pasos a seguir son los siguientes:

1.- Ejecute el script "createdatabase.sh".

En alternativa, ejecute en el directorio "code" el siguiente comando:

    spatialite incidencias.sqlite < incidencias.sql

2.- Ejecute el script "testdatabase.sh".

En alternativa, ejecute en el directorio "code" el siguiente comando:

	spatialite incidencias.sqlite < test.sql

Si la salida es igual a la siguiente, el proceso se ha concluido
satisfactoriamente y se dispone de una base de datos Spatialite correcta
para su uso con la aplicación en el fichero "incidencias.sqlite":

	1460328181697|Inundación|200|2016-04-10T22:30+02:00|2016-05-10T22:30+02:00|
		{"type":"Point","coordinates":[-8.737299999999999,42]}

3.- Ejecute el script "startserver.sh".

En alternativa, ejecute en el directorio "code" el siguiente comando:

	python2.7 -m CGIHTTPServer 8000

4.- Abra en un navegador la siguiente dirección:

	http://localhost:8000/panelmunicipal.html

