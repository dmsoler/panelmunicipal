BEGIN;

CREATE TABLE Incidencias (
 id INTEGER NOT NULL PRIMARY KEY,
 tipo TEXT(50),
 radio INTEGER,
 inicio TEXT(22),
 fin TEXT(22)
);

SELECT AddGeometryColumn('Incidencias', 'geometry', 4326, 'POINT', 'XY', 1);

COMMIT;

