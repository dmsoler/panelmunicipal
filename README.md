# Panel municipal:

# Aplicación basada en web y en software libre para la gestión municipal de incidencias y actuaciones en el espacio público

Trabajo de fin de Grado en Tecnologías de Telecomunicación presentado en la Universidad Oberta de Catalunya el 6 de junio de 2016.

Publicado en <http://hdl.handle.net/10609/53643>

## Resumen

El presente trabajo describe el proceso de desarrollo de una aplicación para la gestión de datos
georreferenciados sobre incidencias en un municipio, utilizando tecnologı́as web, software
libre y datos abiertos. La aplicación consta de una interfaz web HTML+CSS+JavaScript que
utiliza las bibliotecas OpenLayers y jQuery, almacena los datos sobre incidencias en una base
de datos SpatiaLite y conecta la interfaz a la base de datos mediante scripts CGI escritos
en Python, utilizando GeoJSON como formato de intercambio de datos. Para disponer de
mapas e imágenes del terreno en la interfaz, se recurre a las ortofotos del Plan Nacional de
Ortofotografı́a Aérea y a los mapas del proyecto OpenStreetMap, y se utilizan datos espaciales
públicos disponibles en los servidores estatales españoles, recurriendo también a GeoJSON
como formato de carga de datos en la interfaz. Además de la visualización y edición del
contenido de la base de datos de incidencias, la aplicación permite también la exportación de
dichos datos en formato KML, para su uso en otras aplicaciones.

## Abstract

This paper describes the development of an application for the management of geospatial
data about incidents or events in a town by the town council. The application uses web
technologies, free software and open data. It consists of a HTML+CSS+JavaScript web interface
that makes use of OpenLayers and jQuery libraries, stores its data about incidents and events in
a SpatiaLite database and connects the interface with the database by using Python CGI scripts
and GeoJSON as geospatial data interchange format. The needed maps are obtained from
OpenStreetMap project, orthophotos from Spanish National Orthophotography Plan (PNOA)
and further public spatial vectorial data from other Spanish public services, using also GeoJSON
as data loading format for the interface. The application allows for the visualization and edition
of incidents and events stored in the spatial database, and also for exporting such data in KML
format for their use in other applications.

## Author

* **Dionisio Martínez Soler**

## License

This project is licensed under the GNU General Public License (GPL) version 3 and the GNU Free Documentation License (FDL) version 1.3 - see the [LICENSE](LICENSE) file for details.
